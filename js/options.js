$(document).ready(function(){
    chrome.storage.sync.get({
        urls: [],
        css: ''
    }, function(items) {
        $('#urls').val(items.urls.join('\n'));
        $('#css').val(items.css);
    });
    $('#save').on('click', function() {
        var urls = $('#urls').val().split(/\n/);
        var css = $('#css').val();
        chrome.storage.sync.set({
            urls: urls,
            css: css
        });
    });
});
