$(document).ready(function() {
    chrome.storage.sync.get({
        urls: [],
            css: ''
    }, function(items) {
        var imports = '';
        var css = items.css;
        for( var i = 0; i < items.urls.length; i++ ) {
            imports += '@import url(\'' + items.urls[i] + '\');';
        }
        $('<style id=\'cssinsert-autostyle\'>"' + imports + css + '"</style>').appendTo('head');
    });
});
