$(document).ready(function() {
    chrome.tabs.executeScript({
        code: '$("#cssinsert-style").remove();'
    });

    $('#cssinsert-textbox').on('input', function() {
	    var t = $('#cssinsert-textbox').val();
	    t = t.replace(/\r?\n/g, '').replace(/"/g, '');
	    chrome.tabs.executeScript({
		code: '$("#cssinsert-style").remove();' +
			'$("<style id=\'cssinsert-style\'>' + t + '</style>").appendTo("head");'
	    });
	    chrome.storage.local.set({
		    'popup': t
	    }, function() {});
	});
	chrome.storage.local.get(null, function(obj) {
		$('#cssinsert-textbox').val(obj.popup);
	});
});
